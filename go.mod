module bitbucket.org/munkeeteam/horus-core

go 1.12

require (
	cloud.google.com/go v0.43.0
	github.com/rwcarlsen/goexif v0.0.0-20190401172101-9e8deecbddbd
	google.golang.org/genproto v0.0.0-20190801165951-fa694d86fc64 // indirect
)
