package detect

import "time"

type ImageGPSInfo struct {
	Longitude float64 `json:"altitude"`
	Latitude  float64 `json:"latitude"`
}

type ImageDeviceInfo struct {
	DeviceName string    `json:"device_name"`
	TakenDate  time.Time `json:"taken_date"`
}

type DetectedData struct {
	Message    string          `json:"message"`
	FullText   []string        `json:"full_text"`
	Texts      []string        `json:"texts"`
	Prices     []string        `json:"prices"`
	Barcodes   []string        `json:"barcodes"`
	Logos      []string        `json:"logos"`
	GPSInfo    ImageGPSInfo    `json:"gps_info"`
	DeviceInfo ImageDeviceInfo `json:"device_info"`
}

//not use public
type Detection struct {
	HasData     bool     `json:"has_data"`
	Length      int      `json:"length"`
	Annotations []string `json:"annotations"`
}
