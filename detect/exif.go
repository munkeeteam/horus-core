package detect

import (
	"fmt"
	"github.com/rwcarlsen/goexif/exif"
	"github.com/rwcarlsen/goexif/mknote"
	"log"
	"os"
)

func detectExif(file string) (ImageGPSInfo, ImageDeviceInfo, error) {
	var gpsInfo ImageGPSInfo
	var deviceInfo ImageDeviceInfo

	f, err := os.Open(file)
	if err != nil {
		log.Fatal(err)
	}

	// Optionally register camera makenote data parsing - currently Nikon and
	// Canon are supported.
	exif.RegisterParsers(mknote.All...)

	x, err := exif.Decode(f)
	if err != nil {
		return ImageGPSInfo{}, ImageDeviceInfo{}, err
	}

	camModel, err := x.Get(exif.Model) // normally, don't ignore errors!
	if err != nil {
		return ImageGPSInfo{}, ImageDeviceInfo{}, err
	}

	camModelVal, err := camModel.StringVal()
	if err != nil {
		return ImageGPSInfo{}, ImageDeviceInfo{}, err
	}

	// Two convenience functions exist for date/time taken and GPS coords:
	tm, err := x.DateTime()
	if err != nil {
		return ImageGPSInfo{}, ImageDeviceInfo{}, err
	}

	fmt.Println("Taken: ", tm)
	deviceInfo = ImageDeviceInfo{
		DeviceName: camModelVal,
		TakenDate:  tm,
	}

	// GPS Info
	lat, long, _ := x.LatLong()
	gpsInfo = ImageGPSInfo{
		Longitude: long,
		Latitude: lat,
	}

	return gpsInfo, deviceInfo, nil
}
