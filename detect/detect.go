package detect

import (
	"fmt"
	"path/filepath"
	"regexp"
	"strings"
)

func Detect(file string, maxResults, barCodeLength int) (DetectedData, error) {
	if strings.ToLower(filepath.Ext(file)) != ".heic" {
		var response DetectedData

		detection, err := detectText(file, maxResults)
		logoDetection, err := detectLogos(file, maxResults)

		if err != nil {
			response.Message = err.Error()
		}

		re_price := regexp.MustCompile("(\\d+\\.\\d{1,2})")
		re_sku := regexp.MustCompile("^[0-9]*$")

		for _, annotation := range detection.Annotations {
			s := strings.Split(annotation, "\n")
			for _, ss := range s {
				if re_price.MatchString(ss) {
					response.Prices = append(response.Prices, ss)
				}
				if re_sku.MatchString(ss) {
					if len(ss) >= barCodeLength {
						response.Barcodes = append(response.Barcodes, ss)
					}
				} else {
					if !strings.Contains(annotation, "\n") {
						response.Texts = append(response.Texts, annotation)
					} else {
						response.FullText = append(response.FullText, annotation)
					}
				}
			}
		}

		// logos annotations
		for _, annotation := range logoDetection.Annotations {
			response.Logos = append(response.Logos, annotation)
		}

		// exif info
		response.GPSInfo, response.DeviceInfo, err = detectExif(file)
		if err != nil {
			response.GPSInfo = ImageGPSInfo{}
			response.DeviceInfo = ImageDeviceInfo{}
		}

		// clean crap
		response.Texts = removeDuplicates(response.Texts)
		response.Prices = removeDuplicates(response.Prices)
		response.Barcodes = removeDuplicates(response.Barcodes)
		response.FullText = removeDuplicates(response.FullText)
		response.Logos = removeDuplicates(response.Logos)

		return response, nil
	} else {
		return DetectedData{}, fmt.Errorf("HEIC not supported")
	}
}

// removes all repeated info
func removeDuplicates(elements []string) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}
