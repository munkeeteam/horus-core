package detect

import (
	"context"
	"os"

	vision "cloud.google.com/go/vision/apiv1"
)

// detectText gets text from the Vision API for an image at the given file path.
func detectText(file string, maxResults int) (Detection, error) {
	var detection Detection
	ctx := context.Background()

	client, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		return Detection{}, err
	}

	f, err := os.Open(file)
	if err != nil {
		return Detection{}, err
	}
	defer f.Close()

	image, err := vision.NewImageFromReader(f)
	if err != nil {
		return Detection{}, err
	}
	annotations, err := client.DetectTexts(ctx, image, nil, maxResults)
	if err != nil {
		return Detection{}, err
	}

	if len(annotations) == 0 {
		detection = Detection{
			HasData:     false,
			Annotations: nil,
		}
		return detection, nil
	} else {
		detection.HasData = true
		detection.Length = len(annotations)
		for _, annotation := range annotations {
			detection.Annotations = append(detection.Annotations, annotation.Description)
		}
	}

	return detection, nil
}

// detectLogos gets logos from the Vision API for an image at the given file path.
func detectLogos(file string, maxResults int) (Detection, error) {
	var detection Detection
	ctx := context.Background()

	client, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		return Detection{}, err
	}

	f, err := os.Open(file)
	if err != nil {
		return Detection{}, err
	}
	defer f.Close()

	image, err := vision.NewImageFromReader(f)
	if err != nil {
		return Detection{}, err
	}
	annotations, err := client.DetectLogos(ctx, image, nil, maxResults)
	if err != nil {
		return Detection{}, err
	}

	if len(annotations) == 0 {
		detection = Detection{
			HasData:     false,
			Annotations: nil,
		}
		return detection, nil
	} else {
		detection.HasData = true
		for _, annotation := range annotations {
			detection.Annotations = append(detection.Annotations, annotation.Description)
		}
	}

	return detection, nil
}

// TODO: Detects on web
/*
func DetectWeb(file string) (Detection, error) {
	var detection Detection
	ctx := context.Background()

	client, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		return Detection{}, err
	}

	f, err := os.Open(file)
	if err != nil {
		return Detection{}, err
	}
	defer f.Close()

	image, err := vision.NewImageFromReader(f)
	if err != nil {
		return Detection{}, err
	}
	web, err := client.DetectWeb(ctx, image, nil)
	if err != nil {
		return Detection{}, err
	}

	fmt.Fprintln(w, "Web properties:")
	if len(web.FullMatchingImages) != 0 {
		fmt.Fprintln(w, "\tFull image matches:")
		for _, full := range web.FullMatchingImages {
			fmt.Fprintf(w, "\t\t%s\n", full.Url)
		}
	}
	if len(web.PagesWithMatchingImages) != 0 {
		fmt.Fprintln(w, "\tPages with this image:")
		for _, page := range web.PagesWithMatchingImages {
			fmt.Fprintf(w, "\t\t%s\n", page.Url)
		}
	}
	if len(web.WebEntities) != 0 {
		fmt.Fprintln(w, "\tEntities:")
		fmt.Fprintln(w, "\t\tEntity\t\tScore\tDescription")
		for _, entity := range web.WebEntities {
			fmt.Fprintf(w, "\t\t%-14s\t%-2.4f\t%s\n", entity.EntityId, entity.Score, entity.Description)
		}
	}
	if len(web.BestGuessLabels) != 0 {
		fmt.Fprintln(w, "\tBest guess labels:")
		for _, label := range web.BestGuessLabels {
			fmt.Fprintf(w, "\t\t%s\n", label.Label)
		}
	}

	return detection, nil
} */
